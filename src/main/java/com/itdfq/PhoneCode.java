package com.itdfq;



import redis.clients.jedis.Jedis;

import java.util.Random;

/**
 * @Author GocChin
 * @Date 2021/5/5 16:52
 * @Blog: itdfq.com
 * @QQ: 909256107
 * @Descript: 输入手机号，点击发送验证码，随机生成6位数的验证码，2分钟内有效，同一个手机号每天只能发送三次
 */
public class PhoneCode {
    public static void main(String[] args) {
       //模拟验证码的发送
//        verifyCode("123434");
        //模拟验证码校验
        getRedisCode("123434","976078");

    }

    //生成6为数字的验证码
    public static String getCode(){
        String code ="";
        Random random = new Random();
        for (int i = 0 ;i<6;i++){
            int rand = random.nextInt(10);
            code+=rand;
        }
        return code;
    }
    //每个手机每天只能发送三次，验证码放到redis中，设置过期时间
    public static void verifyCode(String phone){
        //创建Jedis对象
        Jedis jedis = new Jedis("你的IP",6379);
        //设置密码
        jedis.auth("你的密码");
        //拼接手机发送次数key
        String countKey ="VerifyCode"+phone+":count";
        //验证码的key
        String  codeKey="VerifyCode"+phone+":code";
        //每个手机每天只能发送三次
        String count = jedis.get(countKey);
        if (count==null){
            //没有发送次数
            //设置发送次数为1
            jedis.setex(countKey,24*60*60,"1");
        }else if(Integer.parseInt(count)<=2){
            //发送次数加一
            jedis.incr(codeKey);
        }else if(Integer.parseInt(count)>2){
            System.out.println("今天的发送次数已经超过三次");
            jedis.close();
        }
        //发送的验证码放到redis里面
        String vcode = getCode();
        //设置验证码有效时间为120秒
        jedis.setex(codeKey,120,vcode);
        jedis.close();

    }

    //验证码校验
    public static void getRedisCode(String phone,String code){
        //创建Jedis对象
        Jedis jedis = new Jedis("你的IP",6379);
        //设置密码
        jedis.auth("你的密码");
        //验证码的key
        String  codeKey="VerifyCode"+phone+":code";
        //获取验证码
        String s = jedis.get(codeKey);
        //判断
        if (s.equals(code)){
            System.out.println("验证码正确");
        }else{
            System.out.println("验证码不正确");
        }
        jedis.close();
    }
}
